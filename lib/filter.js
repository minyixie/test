exports = module.exports = {};

exports.parse = function(input){

	var out_put = {'response':[]};

	for(var i=0;i<input.payload.length;i++){
		if ( input.payload[i].hasOwnProperty('drm') && input.payload[i].drm==true 
			  && input.payload[i].hasOwnProperty('episodeCount') && input.payload[i].episodeCount>0){
			var push_data = {'image':input.payload[i].image.showImage,'slug':input.payload[i].slug,'title':input.payload[i].title};
			out_put['response'].push(push_data);
		}
	}
	return out_put;
}