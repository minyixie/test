//variables
var express = require('express');
var bodyParser = require('body-parser');
var filter = require("./lib/filter");

var app = express();

//port 
app.set('port', (process.env.PORT || 5000));

//using bodyParser
app.use(bodyParser.json());

//err
app.use(function(err, req, res, next) {
  if (err){
	res.status(err.status).send({'error':'Could not decode request: JSON parsing failed'});
  }
});

//post
app.post('/', function(request, response) {
  	var jason_object;
	try{
		jason_object = request.body;
		var out_put = filter.parse(jason_object);
		response.send(out_put);
	}catch(err){
		var out_err = {'error':'error in data.'};
		response.send(out_err);
	}
});

//listen
app.listen(app.get('port'), function() {
})
